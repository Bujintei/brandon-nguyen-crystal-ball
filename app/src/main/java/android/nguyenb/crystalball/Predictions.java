package android.nguyenb.crystalball;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "What do you want?",
                "Ask again later~",
                "You will die a nice, slow, painful death"
        };
    }

    public static Predictions get() {
        if (predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }

    public  String getPrediction() {
        return answers [2];
    }

}
